from rest_framework.routers import SimpleRouter

from .views import PlayerViewSet, TeamViewSet

app_name = 'sports'

player_router = SimpleRouter()
player_router.register('player', PlayerViewSet, basename='player')

team_router = SimpleRouter()
team_router.register('team', TeamViewSet, basename='team')

urlpatterns = player_router.urls + team_router.urls
