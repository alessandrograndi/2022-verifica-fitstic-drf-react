from rest_framework import serializers
from .models import Player, Team

__all__ = ['PlayerSerializer', 'TeamSerializer']


PlayerSerializer = None

TeamSerializer = None
