from django.db import models

__all__ = ['Player', 'Team']


class Team(models.Model):
    name = models.CharField(max_length=512)
    description = models.CharField(max_length=4096)

    def __str__(self):
        return self.name


class Player(models.Model):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)

    team = models.ForeignKey(Team, on_delete=models.PROTECT, related_name="players")

    def __str__(self):
        return f"{self.first_name} {self.last_name}"
