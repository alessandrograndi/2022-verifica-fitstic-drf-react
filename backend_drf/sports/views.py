from rest_framework import viewsets

from .models import Player, Team
from .serializers import PlayerSerializer, TeamSerializer

__all__ = ['PlayerViewSet', 'TeamViewSet']


PlayerViewSet = None

TeamViewSet = None
