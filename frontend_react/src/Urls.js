
const apiUrls = {
    login: "http://localhost:8000/api/dj-rest-auth/login/",
    player: "http://localhost:8000/api/v1/player/",
    team: "http://localhost:8000/api/v1/team/",
}

export default apiUrls;
