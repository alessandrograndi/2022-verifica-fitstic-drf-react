import PropTypes from 'prop-types';
import { FaTrash } from "react-icons/fa";
import React from "react";
import axios from "axios";
import apiUrls from "../Urls";



const TeamItem = (props) => {
    const deleteTeam = async(id, token) => {
        await axios.delete(
            apiUrls.team + id + "/",
            {headers: {'Authorization': `Token ${token}`}}
            )
            .catch(
            err => { alert(err) }
            );
        props.refresh_all();
    }
    return (
        <div className="item">
            Add name, description and FaTrash icon
        </div>
    )
}

TeamItem.propTypes = {
    team: PropTypes.object,
}
export default TeamItem