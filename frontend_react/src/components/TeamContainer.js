import React from 'react'
import axios from "axios";
import TeamItem from "./TeamItem";
import "./Team.css";
import {FaClock, FaCross, FaPlus} from "react-icons/fa";
import AddTeam from "./AddTeam";
import apiUrls from "../Urls";

class TeamContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            teams: [],
            new_team: false
        }
    }

    componentDidMount() {
        this.getTeams()
    }

    getTeams() {

    }

    toggleAddTeam() {
        this.setState({
            ...this.state,
            new_team: !this.state.new_team
        })
    }

    render() {
        return (
            <div className="container box">
                <h2 className="container text-center">
                    {this.state.new_team ? "Add Team" : "Team List"}
                </h2>
                <hr/>

                {
                    this.state.new_team ?
                        (
                            <>
                                <FaClock
                                    className="container text-center m-3"
                                    onClick={() => {
                                        this.toggleAddTeam()
                                    }}
                                />
                                <AddTeam
                                    token={this.props.token}
                                    toggle={this.toggleAddTeam.bind(this)}
                                    refresh_all={this.getTeams.bind(this)}
                                />
                            </>
                        )
                        : (
                            <>
                                {this.state.teams.map(team => (
                                    <TeamItem
                                        key={team.id}
                                        team={team}
                                        token={this.props.token}
                                        refresh_all={this.getTeams.bind(this)}
                                    />
                                ))}
                                <FaPlus
                                    className="container text-center m-3"
                                    onClick={() => {
                                        this.toggleAddTeam()
                                    }}
                                />
                            </>
                        )
                }
            </div>
        )
    }
}

export default TeamContainer