import Button from "react-bootstrap/Button";
import {Link} from "react-router-dom";

const Header = () => {
    return (
        <header className="container my-3">
            <Link to="/">
                <Button className="ms-2" variant="info">Home</Button>
            </Link>
            <Link to="/team">
                <Button className="ms-2" variant="info">Team</Button>
            </Link>
            <Link to="/player">
                <Button className="ms-2" variant="info">Player</Button>
            </Link>
        </header>
    );
}

export default Header
