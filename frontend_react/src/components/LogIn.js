import React from 'react';
import axios from "axios";
import Button from 'react-bootstrap/Button'
import { useNavigate } from "react-router-dom";
import apiUrls from "../Urls";


class LogIn extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
        }
    }

    logIn(event) {
        event.preventDefault();
        console.log(this.state);
        axios.post(
            apiUrls.login,
            this.state
        ).then(
            res => {
                this.props.updateToken(res.data.key)
            }
        ).catch(err => {
            alert(err)
        });
    }

    render() {
        return (
            <div className="login">
                {
                    this.props.token ?
                        (
                            <p>Already logged in: {this.props.token}</p>
                        ) : (
                            <form method="post" onSubmit={(event => {
                                this.logIn(event)
                            })}>
                                <div className="form-row d-flex align-items-center justify-content-around">
                                    <div>
                                        <label>Username</label>
                                        <input className="form-control" type="text"
                                               placeholder="Username" value={this.state.username}
                                               onChange={(e) => this.setState({
                                                   username: e.target.value
                                               })}/>
                                    </div>
                                </div>
                                <div className="form-row d-flex align-items-center justify-content-around mt-3">
                                    <div>
                                        <label>Password</label>
                                        <input className="form-control" type="password"
                                               placeholder="Password" value={this.state.password}
                                               onChange={(e) => this.setState({
                                                   password: e.target.value
                                               })}/>
                                    </div>
                                </div>
                                <div className="d-flex align-items-center justify-content-end">
                                    <Button className="mt-3" variant="danger" type="submit">
                                        Login
                                    </Button>
                                </div>
                            </form>

                        )

                }
            </div>
        )
    }
}

export default LogIn