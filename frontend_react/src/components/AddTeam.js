import React from 'react';
import axios from 'axios'
import {FaPlus} from "react-icons/fa";
import apiUrls from "../Urls";
import Button from "react-bootstrap/Button";

class AddTeam extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            description: '',
        }
    }

    async addTeam(event) {
        event.preventDefault();
        await axios.post(
            apiUrls.team,
            this.state,
            {headers: {'Authorization': `Token ${this.props.token}`}}
        ).then(res => {this.props.toggle();})
            .catch(err => { alert(err) });
        this.props.refresh_all();
        this.setState({
            name: '',
            description: '',
        });
    }

    render() {
        return (
            <form action="" className="container form">
                <p>
                    <label htmlFor="name">
                        Nome:
                    </label>
                    <input
                        type="text" name="name" id="name"
                        className="form-control"
                        value={this.state.name}
                        onChange={(e) => {
                            this.setState({name: e.target.value})
                        }}
                    />
                </p>
                <p>
                    Implement description
                </p>
                <p className="text-center">
                    <Button className="ms-2" variant="info"
                        onClick={(e) => {
                            this.addTeam(e)
                        }}
                    >Create</Button>
                </p>
            </form>
        )
    }
}
export default AddTeam