import React from 'react'
import {BrowserRouter as Router, Route, Routes} from "react-router-dom";
import Header from "./components/Header";
import PlayerContainer from "./components/PlayerContainer";
import TeamContainer from "./components/TeamContainer";
import LogIn from "./components/LogIn";
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            token: null
        }
    }

    updateToken(token) {
        this.setState({token: token});
    }

    render() {
        return (
            <Router>
                <Header/>
                <div className="App">
                    <Routes>
                        <Route path="/player" element={<PlayerContainer/>}/>
                        <Route path="/team" element={<TeamContainer token={this.state.token}/>}/>
                        <Route path="/"
                               element={<LogIn token={this.state.token} updateToken={this.updateToken.bind(this)}/>}/>
                    </Routes>
                </div>
            </Router>
        );
    }

}

export default App;
